package com.tools.meditation.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tools.meditation.NoiseListCreator;
import com.tools.meditation.R;
import com.tools.meditation.activities.MainActivity;
import com.tools.meditation.model.Noise;
import com.tools.meditation.utils.mediaplayer.MediaPlayerController.PlayResult;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CategoryFragment extends Fragment {

    public ListAdapter adapter;
    public RecyclerView recNoises;
    int[] firstPosArr = new int[]{0, 3, 6, 9};
    int[] secondPosArr = new int[]{1, 4, 7, 10};
    int[] thirdPosArr = new int[]{2, 5, 8, 11};
    List<Integer> firstColumnList = new ArrayList<>();
    List<Integer> secondColumnList = new ArrayList<>();
    List<Integer> thirdColumnList = new ArrayList<>();
    private GridLayoutManager lLayout;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            adapter.notifyDataSetChanged();
        }
    };

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category, container, false);
        setupView(rootView);
        createPosArrList(firstColumnList, firstPosArr);
        createPosArrList(secondColumnList, secondPosArr);
        createPosArrList(thirdColumnList, thirdPosArr);
        return rootView;
    }

    private void createPosArrList(List<Integer> firstColumnList, int[] firstPosArr) {
        for (int i = 0; i < firstPosArr.length; i++) {
            firstColumnList.add(firstPosArr[i]);
        }
    }

    private void setupView(View view) {
        recNoises = view.findViewById(R.id.rec_noise);
        lLayout = new GridLayoutManager(getActivity(), 4);
        recNoises.setHasFixedSize(true);
        recNoises.setLayoutManager(lLayout);
        int position = getArguments().getInt("position");
        adapter = new ListAdapter(getNoiseList(position), position);
        recNoises.setAdapter(adapter);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("stop_all");
        intentFilter.addAction("volume_changed");
        intentFilter.addAction("sound_stoped");
        intentFilter.addAction("favorite_play");
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver, intentFilter);
    }

    private List<Noise> getNoiseList(int position) {
        switch (position) {
            case 0:
                return NoiseListCreator.createRainList();
            case 1:
                return NoiseListCreator.createOceanList();
            case 2:
                return NoiseListCreator.createWindAndFireList();
            case 3:
                return NoiseListCreator.createCountrysideList();
            case 4:
                return NoiseListCreator.createOrientalList();
            case 5:
                return NoiseListCreator.createNightList();
            case 6:
                return NoiseListCreator.createCityList();
            case 7:
                return NoiseListCreator.createHomeList();
            case 8:
                return NoiseListCreator.createRelaxingList();
            case 9:
                return NoiseListCreator.createRiverList();
            default:
                return NoiseListCreator.createRainList();
        }
    }

    public void onDestroy() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

        public MainActivity mainActivity;
        int fragPos;
        private List<Noise> mData;

        public ListAdapter(List<Noise> data, int fragPos) {
            this.mainActivity = (MainActivity) getActivity();
            this.mData = data;
            this.fragPos = fragPos;
        }

        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_noise, parent, false);
            return new ViewHolder(layoutView);
        }

        public void onBindViewHolder(final ViewHolder holder, int position) {
            final Noise item = mData.get(position);

            setTopBottomSpace(holder, position);

            if (mainActivity.isSoundPlaying(item)) {
                holder.imvIcon.setBackgroundResource(R.drawable.app_select_default);
                //holder.tvName.setTextColor(getResources().getColor(R.color.color_white));
            } else {
               //holder.imvIcon.setBackgroundColor(getResources().getColor(R.color.Beige));
                holder.imvIcon.setBackgroundResource(R.drawable.app_selected_back);
                //holder.tvName.setTextColor(getResources().getColor(R.color.color_white));
            }
            holder.tvName.setText(item.name);
            holder.imvIcon.setImageResource(item.iconResId);
            holder.itemView.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    PlayResult result = mainActivity.play(item);
                    if (result == PlayResult.PLAY) {
                        holder.imvIcon.setBackgroundResource(R.drawable.app_select_color);
                        Typeface face = Typeface.createFromAsset(getContext().getAssets(),
                                "fonts/bold.ttf");
                        holder.tvName.setTypeface(face);
                    } else if (result == PlayResult.STOP) {
                        holder.imvIcon.setBackgroundResource(R.drawable.app_selected_pause);
                        Typeface face = Typeface.createFromAsset(getContext().getAssets(),
                                "fonts/medium.ttf");
                        holder.tvName.setTypeface(face);
                    }
                }
            });
        }

        private void setTopBottomSpace(ViewHolder holder, int position) {

        }

        public int getItemCount() {
            return mData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView imvIcon;
            public TextView tvName;

            public ViewHolder(View itemView) {
                super(itemView);
                imvIcon = itemView.findViewById(R.id.imv_icon);
                tvName = itemView.findViewById(R.id.tv_name);
            }
        }
    }
}

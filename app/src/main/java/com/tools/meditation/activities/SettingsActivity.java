package com.tools.meditation.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.SwitchCompat;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.iarcuschin.simpleratingbar.SimpleRatingBar.OnRatingBarChangeListener;
import com.tools.meditation.BuildConfig;
import com.tools.meditation.R;
import com.tools.meditation.utils.CommonUtils;
import com.tools.meditation.utils.Constants;
import com.tools.meditation.utils.Pref;

public class SettingsActivity extends BaseActivity {
    public View imvBack;
    View lnRating;
    SimpleRatingBar ratingBar;
    public SwitchCompat swNoti;
    public TextView tvRemoveAds;

    public static void startActivity(MainActivity act) {
        act.startActivity(new Intent(act, SettingsActivity.class));
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        imvBack = findViewById(R.id.imv_back);
        lnRating = findViewById(R.id.ln_app_rating);
        ratingBar = findViewById(R.id.rating_bar);
        swNoti = findViewById(R.id.sw_active_noti);
        tvRemoveAds = findViewById(R.id.tv_remove_ads);
        tvRemoveAds.setVisibility(View.GONE);
        imvBack.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });
        swNoti.setChecked(Pref.getBoolean(this, Constants.PREF_NOTIFICATION_ACTIVE, true));
        swNoti.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Pref.setBoolean(SettingsActivity.this, Constants.PREF_NOTIFICATION_ACTIVE, isChecked);
            }
        });
        if (!Pref.getBoolean(this, "is_rating", false)) {
            lnRating.setVisibility(View.VISIBLE);
            ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
                public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                    if (rating < 5.0f) {
                        Toast.makeText(SettingsActivity.this, "Thanks for your rating !", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(SettingsActivity.this, "Rate Sleep Machine on Google play !", Toast.LENGTH_LONG).show();
                        CommonUtils.openGGplay(SettingsActivity.this, BuildConfig.APPLICATION_ID);
                    }
                    Pref.setBoolean(SettingsActivity.this, "is_rating", true);
                    lnRating.setVisibility(View.GONE);
                }
            });
        }
    }

}

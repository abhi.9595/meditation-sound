package com.tools.meditation.activities;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.tools.meditation.R;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity {

   /* public void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/

    public void addFragment(int id, Class<? extends Fragment> fmClass) {
        try {
            Fragment fragment = (Fragment) fmClass.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(id, fragment);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static boolean checkConnection(Context mContext) {
        ConnectivityManager conMgr = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            return false;
        } else {
            return true;
        }
    }
//    public void loadBannerAds(final RelativeLayout adContainerView) {
//        //banner ads if internet is on than it will show
//        if (checkConnection(this)) {
//            AdView mAdView = new AdView(this);
//            mAdView.setAdUnitId(getString(R.string.admob_banner_ads_id));
//            adContainerView.
//            adContainerView.addView(mAdView);
//
//            AdSize adSize = getAdSize(this);
//            mAdView.setAdSize(adSize);
//
//           { AdRequest adRequest = new AdRequest.Builder().build();
////            mAdView.loadAd(adRequest);
////            mAdView.setAdListener(new AdListener()
//                @Override
//                public void onAdFailedToLoad(LoadAdError loadAdError) {
//                    //super.onAdFailedToLoad(loadAdError);
//                    Log.e("ADSTAG", "Banner onAdFailedToLoad()" + loadAdError.getCode());
//                    adContainerView.setVisibility(View.GONE);
//                }
////
//                @Override
//                public void onAdLoaded() {
//                    // Code to be executed when an ad finishes loading.
//                    Log.e("ADSTAG", "Banner onAdLoaded()");
//                    adContainerView.setVisibility(View.VISIBLE);
//                }
//
//                /* @Override
//                 public void onAdFailedToLoad(LoadAdError loadAdError) {
//                     // Code to be executed when an ad request fails.
//                     Log.e("ADSTAG", "Banner onAdFailedToLoad()" + loadAdError.getCode());
//                 }
//*/
//                @Override
//                public void onAdOpened() {
//                    // Code to be executed when the ad is displayed.
//                    Log.e("ADSTAG", "Banner onAdOpened()");
//                }
////
////
//                @Override
//                public void onAdClosed() {
//                    // Code to be executed when the banner ad is closed.
//                    Log.e("ADSTAG", "Banner onAdClosed()");
//                }
//            });
//        } else {
//            adContainerView.setVisibility(View.GONE);
//        }
//
//
//    }

    private AdSize getAdSize(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;
        int adWidth = (int) (widthPixels / density);
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

}

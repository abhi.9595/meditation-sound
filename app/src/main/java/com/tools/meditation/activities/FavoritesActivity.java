package com.tools.meditation.activities;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tools.meditation.R;
import com.tools.meditation.model.Favorite;
import com.tools.meditation.model.Noise;
import com.tools.meditation.services.MediaPlayerService;
import com.tools.meditation.services.MediaPlayerService.LocalBinder;
import com.tools.meditation.utils.CommonUtils;
import com.tools.meditation.utils.Constants;
import com.tools.meditation.utils.Pref;

import java.util.Collections;
import java.util.List;

public class FavoritesActivity extends BaseActivity implements OnClickListener {
    public View imvBack;
    public View progress;
    public RecyclerView recFavorite;
    boolean mBound = false;
    MediaPlayerService mService;
    private FavoriteAdapter adapter;
    private boolean isGeneratedFavorite;
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            LocalBinder binder = (LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            fillData();
        }

        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    public static void startActivity(MainActivity act) {
        act.startActivity(new Intent(act, FavoritesActivity.class));
    }


    @SuppressLint("StaticFieldLeak")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_list);
        imvBack = findViewById(R.id.imv_back);
        progress = findViewById(R.id.progress);
        recFavorite = findViewById(R.id.rec_favorite);
        imvBack.setOnClickListener(this);
        isGeneratedFavorite = Pref.getBoolean(this, Constants.PREF_IS_GENERATED_FAVORITE, false);
        if (!isGeneratedFavorite) {
            new AsyncTask<Void, Void, Void>() {

                public void onPreExecute() {
                    super.onPreExecute();
                    progress.setVisibility(View.VISIBLE);
                }


                public Void doInBackground(Void... voids) {
                    createDefaultFavorites();
                    return null;
                }


                public void onPostExecute(Void aVoid) {
                    progress.setVisibility(View.GONE);
                    isGeneratedFavorite = true;
                    Pref.setBoolean(FavoritesActivity.this, Constants.PREF_IS_GENERATED_FAVORITE, isGeneratedFavorite);
                    bindService(new Intent(FavoritesActivity.this, MediaPlayerService.class), mConnection, BIND_AUTO_CREATE);
                    super.onPostExecute(aVoid);
                }
            }.execute(new Void[0]);
        }
    }


    public void onStart() {
        super.onStart();
        if (isGeneratedFavorite) {
            bindService(new Intent(this, MediaPlayerService.class), mConnection, BIND_AUTO_CREATE);
        }
    }


    public void onStop() {
        if (mBound) {
            unbindService(mConnection);
        }
        super.onStop();
    }


    public void fillData() {
        recFavorite.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FavoriteAdapter();
        recFavorite.setAdapter(adapter);
    }


    public String getPlaylistName() {
        StringBuilder builder = new StringBuilder();
        for (Noise n : mService.getPlaylist()) {
            builder.append(n.name).append(",");
        }
        builder.deleteCharAt(builder.length() - 1);
        return builder.toString();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_back:
                onBackPressed();
                return;
            default:
                return;
        }
    }

    public void onBackPressed() {
        if (isGeneratedFavorite) {
            super.onBackPressed();
        }
    }


    public void saveFavorite(Favorite favorite) {
        if (favorite.getId() == null) {
            for (Noise n : this.mService.getPlaylist()) {
                new Noise(n.name, n.iconResId, n.soundResId, n.volume, favorite.uniqueId).save();
            }
        }
        favorite.save();
    }

    private void createDefaultFavorites() {
        Favorite f = new Favorite("A relaxing coffee");
        saveGenerateFavorite(f, //Resource ID #0x7f0b0007
                new Noise("Musician", R.drawable.icon_city_musician, R.raw.city_musician, 50, f.uniqueId),
                new Noise("Coffee shop", R.drawable.icon_city_coffee_shop, R.raw.city_coffee_shop, 50, f.uniqueId));

        Favorite f2 = new Favorite("Peace of the night");
        saveGenerateFavorite(f2,
                new Noise("Violin", R.drawable.icon_music_violin, R.raw.music_violin, 50, f2.uniqueId),
                new Noise("Crickets", R.drawable.icon_nature_night_cricket, R.raw.nature_night_crickets, 50, f2.uniqueId),
                new Noise("Owls", R.drawable.icon_nature_night_owl, R.raw.nature_night_owls, 50, f2.uniqueId));

        Favorite f3 = new Favorite("Marine dream");
        saveGenerateFavorite(f3,
                new Noise("Calm waves", R.drawable.icon_ocean_gentle_waves, R.raw.ocean_calm_waves, 50, f3.uniqueId),
                new Noise("Campfire", R.drawable.icon_fire_campfire, R.raw.fire_campfire, 50, f3.uniqueId),
                new Noise("Piano", R.drawable.icon_music_piano, R.raw.music_piano, 50, f3.uniqueId));

        Favorite f4 = new Favorite("Contact with nature");
        saveGenerateFavorite(f4,
                new Noise("Creek", R.drawable.icon_water_creek, R.raw.water_creek, 50, f4.uniqueId),
                new Noise("Blackbirds", R.drawable.icon_nature_day_birds, R.raw.nature_day_blackbirds, 50, f4.uniqueId),
                new Noise("Harp", R.drawable.icon_music_harp, R.raw.music_harp, 50, f4.uniqueId));

        Favorite f5 = new Favorite("Asian journey");
        saveGenerateFavorite(f5,
                new Noise("Guzheng", R.drawable.icon_oriental_string, R.raw.oriental_string, 50, f5.uniqueId),
                new Noise("Brook", R.drawable.icon_water_brook, R.raw.water_brook, 50, f5.uniqueId),
                new Noise("Blackbirds", R.drawable.icon_nature_day_birds, R.raw.nature_day_blackbirds, 50, f5.uniqueId));

        Favorite f6 = new Favorite("Surfing among dolphins");
        saveGenerateFavorite(f6,
                new Noise("Dolphins", R.drawable.icon_ocean_dolphin, R.raw.ocean_dolphins, 50, f6.uniqueId),
                new Noise("Saiboart", R.drawable.icon_ocean_sailboat, R.raw.ocean_sailboat, 50, f6.uniqueId));

        Favorite f7 = new Favorite("Relaxation on the beach");
        saveGenerateFavorite(f7,
                new Noise("Waves", R.drawable.icon_ocean_waves, R.raw.ocean_waves, 50, f7.uniqueId),
                new Noise("Seagulls", R.drawable.icon_ocean_seagulls, R.raw.ocean_seagulls, 50, f7.uniqueId));

        Favorite f8 = new Favorite("Rainy day");
        saveGenerateFavorite(f8,
                new Noise("Morning rain", R.drawable.icon_rain_morning, R.raw.rain_morning_rain, 50, f8.uniqueId),
                new Noise("Thunder", R.drawable.icon_rain_thunders, R.raw.rain_thunders, 50, f8.uniqueId));

        Favorite f9 = new Favorite("Relaxing rain");
        saveGenerateFavorite(f9,
                new Noise("Light rain", R.drawable.icon_rain_light, R.raw.rain_light, 50, f9.uniqueId),
                new Noise("Piano", R.drawable.icon_music_piano, R.raw.music_piano, 50, f9.uniqueId));
    }

    private void saveGenerateFavorite(Favorite f, Noise... ls) {
        f.save();
        for (Noise n : ls) {
            n.save();
        }
    }

    public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {
        List<Favorite> mData = Favorite.listAll(Favorite.class);
        boolean requestedFocus;

        public FavoriteAdapter() {
            Collections.reverse(this.mData);
            if (!mService.isPlaying() || mService.favoritePlaying != null) {
                this.requestedFocus = true;
            } else {
                this.mData.add(0, new Favorite(getPlaylistName()));
            }
        }

        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorite, parent, false));
        }

        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final Favorite favorite = mData.get(position);
            holder.edtName.setText(favorite.name);
            holder.edtName.setSelection(holder.edtName.getText().length());
            if (mService.favoritePlaying == null || !favorite.uniqueId.equals(mService.favoritePlaying)) {
                holder.imvStatus.setImageResource(R.drawable.nav_play);
            } else {
                holder.imvStatus.setImageResource(R.drawable.nav_stop);
            }
            holder.imvStatus.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    List<Noise> ls = Noise.getFavoriteNoise(favorite.uniqueId);
                    if (ls != null && !ls.isEmpty()) {
                        mService.playFavorite(favorite.uniqueId, ls);
                        notifyDataSetChanged();
                    }
                }
            });
            holder.edtName.setOnFocusChangeListener(new OnFocusChangeListener() {
                public void onFocusChange(View view, boolean b) {
                    if (b) {
                        holder.edtName.setCursorVisible(true);
                        holder.imvStatus.setImageResource(R.drawable.apply_ic);
//                        holder.edtName.setTextColor(ContextCompat.getColor(FavoritesActivity.this, R.color.hint));
                        holder.imvStatus.setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                CommonUtils.hideSoftKeyboard(FavoritesActivity.this);
                                holder.edtName.setTextColor(ContextCompat.getColor(FavoritesActivity.this, R.color.text_dark));
                                holder.edtName.clearFocus();
                                favorite.name = holder.edtName.getText().toString();
                                saveFavorite(favorite);
                                if (mService.favoritePlaying == null && position == 0) {
                                    mService.favoritePlaying = favorite.uniqueId;
                                }
                                notifyDataSetChanged();
                            }
                        });
                        return;
                    }
                    holder.imvStatus.setImageResource(R.drawable.nav_play);
                    holder.edtName.setTextColor(ContextCompat.getColor(FavoritesActivity.this, R.color.text_dark));
                    holder.edtName.setCursorVisible(false);
                    holder.imvStatus.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            List<Noise> ls = Noise.getFavoriteNoise(favorite.uniqueId);
                            if (ls != null && !ls.isEmpty()) {
                                mService.playFavorite(favorite.uniqueId, ls);
                                notifyDataSetChanged();
                            }
                        }
                    });
                }
            });
            if (!requestedFocus && position == 0) {
                holder.edtName.requestFocus();
                requestedFocus = true;
            }
            holder.imvRemove.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (favorite.getId() != null) {
                        favorite.deleteFavorite();
                    }
                    CommonUtils.hideSoftKeyboard(FavoritesActivity.this);
                    holder.edtName.clearFocus();
                    mData.remove(favorite);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, getItemCount());
                }
            });
        }

        public int getItemCount() {
            return mData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public EditText edtName;
            public ImageView imvStatus;
            public ImageView imvRemove;

            public ViewHolder(View itemView) {
                super(itemView);
                edtName = itemView.findViewById(R.id.tv_name);
                imvStatus = itemView.findViewById(R.id.imv_status);
                imvRemove = itemView.findViewById(R.id.imv_remove);
            }
        }
    }
}

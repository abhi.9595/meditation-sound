package com.tools.meditation.activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.shawnlin.numberpicker.NumberPicker;
import com.tools.meditation.R;
import com.tools.meditation.fragments.CategoryFragment;
import com.tools.meditation.model.Noise;
import com.tools.meditation.services.MediaPlayerService;
import com.tools.meditation.services.MediaPlayerService.CountdownListener;
import com.tools.meditation.services.MediaPlayerService.LocalBinder;
import com.tools.meditation.utils.CommonUtils;
import com.tools.meditation.utils.Constants;
import com.tools.meditation.utils.InterstitialAdController;
import com.tools.meditation.utils.LogUtils;
import com.tools.meditation.utils.Pref;
import com.tools.meditation.utils.customTabLayout.SmartTabLayout;
import com.tools.meditation.utils.mediaplayer.MediaPlayerController.PlayResult;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements OnClickListener, CountdownListener {
    //    String GameID = "4652479";
//    String InterID = "MX_player_interstitial";
//    Boolean TestMode = false;
    PagerAdapter adapter;
    static AudioManager audioManager;

    ImageView imvhome;
    ImageView imvPlay;
    ImageView imvPlayingList;
    ImageView imvSettings;



    View layoutTimer;
    boolean mBound = false;
    MediaPlayerService mService;
    static AlertDialog playListDialog;
    MediaButtonReceiver receiver = new MediaButtonReceiver();
    static SeekBar seekVolume;
    SmartTabLayout tabs;
    View timeIcon;
    ImageView ocean_category;
    ImageView windandfire_category;
    ImageView countryside_category;
    ImageView oriental_category;
    ImageView night_category;
    ImageView rain_category;



//    GridLayout mainGrid;

    TextView tvPlayingCount;
    TextView tvTimer;
    ViewPager viewPager;
    String[] titles = {"Rain", "Ocean", "Wind & fire", "Countryside", "Oriental", "Night", "City", "Home", "Relaxing", "River"};
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            LocalBinder binder = (LocalBinder) service;
            mService = binder.getService();
            mBound = true;
//            setupViews();
        }

        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
    private int noiseH;
    private int noiseSpacing;
    private int i;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories);


//
//        mainGrid =(GridLayout)(findViewById(R.id.main_Grid));
//
//        //SET EVENT
//        setSingleEvent(mainGrid);


        imvhome = findViewById(R.id.imv_home);
        //imvPlay = findViewById(R.id.imv_play);
        viewPager = findViewById(R.id.pager_category);
        tvTimer = findViewById(R.id.tv_timer);
        tvPlayingCount = findViewById(R.id.tv_playing_count);
        timeIcon = findViewById(R.id.imv_timer);
        tabs = findViewById(R.id.tabs);
        mAdView = findViewById(R.id.adView);
        layoutTimer = findViewById(R.id.layout_category);
        imvSettings = findViewById(R.id.imv_more);
        imvPlayingList = findViewById(R.id.imv_playing_list);
        ocean_category = findViewById(R.id.Ocean);
        windandfire_category = findViewById(R.id.windandfire);
        countryside_category = findViewById(R.id.CountrySide);
        oriental_category = findViewById(R.id.Oriental);
        night_category = findViewById(R.id.Night);
        rain_category = findViewById(R.id.Rain);




//        raincategory = findViewById(R.id.Rain);




        Intent intent = new Intent(this, MediaPlayerService.class);
        startService(intent);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);




//        raincategory.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent1 = new Intent(MainActivity.this,raincategory.class);
//                startActivity(intent1);
//            }
//        }



        ocean_category.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,ocean_category.class);
                //startActivity(intent1);
                startActivity(i);
            }
        });

        oriental_category.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent4 = new Intent(MainActivity.this, oriental_category.class);
                startActivity(intent4);
            }
        });



        windandfire_category.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(MainActivity.this,windandfire_category.class);
                //startActivity(intent1);
                startActivity(intent2);
            }
        });
        countryside_category.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(MainActivity.this, countryside_category.class);
                startActivity(intent3);
            }
        });
        night_category.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent5 = new Intent(MainActivity.this, night_category.class);
                startActivity(intent5);
            }
        });
        rain_category.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(MainActivity.this,rain_category.class);
                startActivity(intent1);
            }
        });



//    }
//        MobileAds.initialize(this, new OnInitializationCompleteListener() {
//            @Override
//            public void onInitializationComplete(InitializationStatus initializationStatus) {
//            }
//        });
//        MobileAds.initialize(this, initializationStatus -> {
//
//
//        });
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                UnityAds.show(MainActivity.
//                                this,
//                        InterID);
//
//            }
//        }, 15000);
//
//        UnityAds.initialize(this, GameID, TestMode);
//        loadInterstiate();
//    }
//    private void loadInterstiate() {
//        if (UnityAds.isInitialized()) {
//            UnityAds.load((InterID));
//        } else {
//
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    UnityAds.load(InterID);
//
//                }
//            }, 5000);
//        }
//    }
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

//        raincategor.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent1 = new Intent(MainActivity.this,rain_categore.class);
//                startActivity(intent1);
//
//            }
//        });



    }

//    private void setSingleEvent(GridLayout mainGrid) {
//        // Loop all child item of Main Grid
//        for(int i = 0;i <mainGrid.getChildCount();i++);
//        {
//            //You can see all child item in CardView , so we just cast object to CardView
//            CardView cardView =(CardView)mainGrid.getChildAt(i);
//            CardView.setOnClickListener(new View.OnClickListener()
//            {
//
//                @Override
//                public void onClick(View view) {
//                    String text;
//                    Toast.makeText(MainActivity.this, text="Clicked at index"+i ,Toast.LENGTH_SHORT).show();
//
//                }
//            });
//
//
//
//        }
//    }


    //        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
//    }
    RelativeLayout mAdView;

    public PlayResult play(Noise noise) {
        PlayResult result = PlayResult.STOP;
        if (mBound) {
            result = mService.play(noise);
            if (result == PlayResult.PLAY) {
                imvPlay.setImageResource(R.drawable.nav_pause);
                InterstitialAdController.getInstance(this).showAds(MainActivity.this);
            } else if (result == PlayResult.OUT_OF_POOL) {
                Toast.makeText(this, "10 sounds can be played at a time", Toast.LENGTH_LONG).show();
            }
            updatePlayingCount();
        }
        return result;
    }

    public boolean isSoundPlaying(Noise noise) {
        if (mBound) {
            return mService.isPlaying(noise);
        }
        return false;
    }


    public void onDestroy() {
        LogUtils.d("__activity", "onDestroy");
        unbindService(mConnection);
        mBound = false;
        super.onDestroy();
    }

    private void updatePlayingCount() {
        int count = mService.getPlayingCount();
        if (count == 0) {
            startCounterFadeoutAnimation();
        } else {
            if (mService.getPlayingCount() == 1 && tvPlayingCount.getVisibility() == View.GONE)
                startCounterFadeInAnimation();
            else
                tvPlayingCount.setText("" + mService.getPlayingCount());
        }
    }

    public void startCounterFadeInAnimation() {
        Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_in);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                tvPlayingCount.setText("" + mService.getPlayingCount());
                tvPlayingCount.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        tvPlayingCount.startAnimation(animation);
    }

    public void startCounterFadeoutAnimation() {
        Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_out);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tvPlayingCount.setText("0");
                tvPlayingCount.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        tvPlayingCount.startAnimation(animation);
    }

    private void goneAds() {
        if (mAdView != null) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mAdView.getLayoutParams();
            params.height = 0;
            params.topMargin = 0;
            mAdView.setLayoutParams(params);
        }
    }

    private void loadAds() {
        if (Pref.getInt(this, Constants.PREF_PURCHASED, 1) == 2) {
            goneAds();
            return;
        }
        InterstitialAdController.getInstance(this).loadAds(this);
        // loadBannerAds(mAdView);
    }


    public void onStart() {
        super.onStart();
        if (Pref.getInt(this, Constants.PREF_PURCHASED, 1) == 2) {
            goneAds();
        }
        registerReceiver(receiver, new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
    }


    public void onStop() {
        if (mBound) {
            unregisterReceiver(receiver);
        }
        super.onStop();
    }


    public void setupViews() {
        loadAds();
        //imvPlay.setOnClickListener(this);
        //layoutTimer.setOnClickListener(this);
        imvhome.setOnClickListener(this);
        imvPlayingList.setOnClickListener(this);
        imvSettings.setOnClickListener(this);
        adapter = new PagerAdapter(getSupportFragmentManager(), this);
        viewPager.setOffscreenPageLimit(titles.length);
        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
        mService.setCountdownListener(this);
        updatePlayingCount();
        if (mService.isPaused()) {
            imvPlay.setImageResource(R.drawable.nav_play);
        } else {
            imvPlay.setImageResource(R.drawable.nav_pause);
        }
        viewPager.post(new Runnable() {
            public void run() {
                viewPager.setCurrentItem(Pref.getInt(MainActivity.this, Constants.PREF_PAGE_SELECTED, 0), false);
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                Pref.setInt(MainActivity.this, Constants.PREF_PAGE_SELECTED, position);
            }

            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_home:
                FavoritesActivity.startActivity(this);
                return;
            case R.id.imv_more:
                SettingsActivity.startActivity(this);
                return;
//            case R.id.imv_play :
//                if (mService.isPaused()) {
//                    if (!mService.resumeAll(false)) {
//                        Toast.makeText(this, "No sounds selected", Toast.LENGTH_LONG).show();
//                        return;
//                    } else {
//                        imvPlay.setImageResource(R.drawable.nav_pause);
//                        return;
//                    }
//                } else if (!mService.pauseAll(false)) {
//                    Toast.makeText(this, "No sounds selected", Toast.LENGTH_LONG).show();
//                    return;
//                } else {
//                    imvPlay.setImageResource(R.drawable.nav_play);
//                    return;
//                }
            case R.id.imv_playing_list:
                showPlayingList();
                return;
            case R.id.layout_category:
                showTimePicker();
                return;
            default:
                return;
        }
    }

    public void setVolume(Noise noise, int volume) {
        mService.setVolume(noise, volume);
    }

    private void showTimePicker() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.dialog_time_picker2);
        final AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(1);
        dialog.show();
        RadioGroup radGroup = (RadioGroup) dialog.findViewById(R.id.rad_group_time);
        radGroup.check(Pref.getInt(this, Constants.PREF_TIME_PICKER_POS, R.id.rad_off));
        for (int i = 0; i < radGroup.getChildCount(); i++) {
            View v = radGroup.getChildAt(i);
            if (v instanceof RadioButton) {
                v.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (((RadioButton) v).isChecked()) {
                            Pref.setInt(MainActivity.this, Constants.PREF_TIME_PICKER_POS, v.getId());
                            switch (v.getId()) {
                                case R.id.min_10:
                                    mService.setCountDownTime(600);
                                    break;
                                case R.id.min_15:
                                    mService.setCountDownTime(900);
                                    break;
                                case R.id.min_1hour:
                                    mService.setCountDownTime(3600);
                                    break;
                                case R.id.min_2hour:
                                    mService.setCountDownTime(7200);
                                    break;
                                case R.id.min_30:
                                    mService.setCountDownTime(1800);
                                    break;
                                case R.id.min_3hour:
                                    mService.setCountDownTime(10800);
                                    break;
                                case R.id.min_5:
                                    mService.setCountDownTime(300);
                                    break;
                                case R.id.rad_off:
                                    mService.setCountDownTime(0);
                                    tvTimer.setVisibility(View.GONE);
                                    timeIcon.setVisibility(View.VISIBLE);
                                    break;
                            }
                            dialog.dismiss();
                        }
                    }
                });
            }
        }
        dialog.findViewById(R.id.tv_cancel).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.tv_custom_timer).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                showCustomTimer();
                dialog.dismiss();
            }
        });
    }


    public void showCustomTimer() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.dialog_custom_time_picker);
        final AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(1);
        dialog.show();
        final NumberPicker hourPicker = (NumberPicker) dialog.findViewById(R.id.hour_picker);
        final NumberPicker minPicker = (NumberPicker) dialog.findViewById(R.id.min_picker);
        dialog.findViewById(R.id.imv_ok).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                mService.setCountDownTime((hourPicker.getValue() * 3600) + (minPicker.getValue() * 60));
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.imv_cancel).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void showPlayingList() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.dialog_playing_list);
        playListDialog = builder.create();
        playListDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        playListDialog.requestWindowFeature(1);
        playListDialog.show();
        if (mService.getPlayingCount() == 0) {
            playListDialog.findViewById(R.id.tv_no_sounds_selected).setVisibility(View.VISIBLE);
        } else {
            RecyclerView recPlaying = playListDialog.findViewById(R.id.rec_playing);
            recPlaying.setLayoutManager(new LinearLayoutManager(this));
            recPlaying.setAdapter(new PlaylistAdapter());
        }
        seekVolume = playListDialog.findViewById(R.id.seek_volume);
        seekVolume.setMax(audioManager.getStreamMaxVolume(3));
        seekVolume.setProgress(audioManager.getStreamVolume(3));
        seekVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                audioManager.setStreamVolume(3, progress, 0);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public void onTick(int seconds) {
        if (seconds == 0) {
            tvTimer.setVisibility(View.GONE);
            timeIcon.setVisibility(View.VISIBLE);
            return;
        }
        tvTimer.setVisibility(View.VISIBLE);
        tvTimer.setText(CommonUtils.secondsToTimeString(seconds));
        timeIcon.setVisibility(View.INVISIBLE);
    }

    public void onPauseAll() {
        imvPlay.setImageResource(R.drawable.nav_play);
    }

    public void onResumeAll() {
        imvPlay.setImageResource(R.drawable.nav_pause);
    }

    public void onStopAll() {
        updatePlayingCount();
    }

    public void onPlayListChanged() {
        updatePlayingCount();
    }


    public class MediaButtonReceiver extends BroadcastReceiver {
        public MediaButtonReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (seekVolume != null && playListDialog.isShowing()) {
                seekVolume.setProgress(audioManager.getStreamVolume(3));
            }
        }
    }

    class PagerAdapter extends FragmentPagerAdapter {
        Context mContext;

        public PagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.mContext = context;
        }

        public Fragment getItem(int position) {
            Fragment fragment = new CategoryFragment();
            Bundle args = new Bundle();
            args.putInt("position", position);
            fragment.setArguments(args);
            return fragment;
        }

        public int getCount() {
            return titles.length;
        }

        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }

    public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.ViewHolder> {
        List<Noise> mData = new ArrayList();

        public PlaylistAdapter() {
            this.mData.addAll(mService.getPlaylist());
        }

        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_playing, parent, false));
        }

        public void onBindViewHolder(ViewHolder holder, int position) {
            Noise noise = mData.get(position);
            holder.imvIcon.setImageResource(noise.iconResId);
            holder.seekVolume.setProgress(Pref.getInt(MainActivity.this, Constants.PREF_PREFIX_VOLUME + noise.soundResId, 100));
        }

        public int getItemCount() {
            return mService.getPlayingCount();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView imvIcon;
            public ImageView imvStop;
            public SeekBar seekVolume;

            public ViewHolder(View itemView) {
                super(itemView);
                imvIcon = itemView.findViewById(R.id.imv_icon);
                imvStop = itemView.findViewById(R.id.imv_stop);
                seekVolume = itemView.findViewById(R.id.seek_volume);
                seekVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        setVolume(mData.get(getLayoutPosition()), seekBar.getProgress());
                        LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(new Intent("volume_changed"));
                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
                imvStop.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        view.setOnClickListener(null);
                        int possss = getLayoutPosition();
                        if (play(mData.get(possss)) == PlayResult.STOP) {
                            mData.remove(possss);
                            notifyItemRemoved(possss);
                            if (mService.getPlayingCount() == 0 && playListDialog != null && playListDialog.isShowing()) {
                                playListDialog.findViewById(R.id.tv_no_sounds_selected).setVisibility(View.VISIBLE);
                            }
                            LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(new Intent("sound_stoped"));
                        }
                    }
                });
            }
        }
    }
}

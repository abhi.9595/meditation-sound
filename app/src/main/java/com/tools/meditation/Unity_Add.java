package com.tools.meditation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;

import com.google.android.gms.ads.MobileAds;
import com.tools.meditation.activities.MainActivity;
import com.tools.meditation.utils.Constants;
import com.unity3d.ads.UnityAds;

public class Unity_Add extends AppCompatActivity {
    String GameID = "4493685";
    String InterID = "Interstitial_Android";
    Boolean TestMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unity__add);


        MobileAds.initialize(this, initializationStatus -> {

        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                UnityAds.show(Unity_Add.
                        this,
                        InterID);

            }
        }, 15000);

        UnityAds.initialize(this, GameID, TestMode);
        loadInterstiate();


    }

    private void loadInterstiate() {
        if (UnityAds.isInitialized()) {
            UnityAds.load((InterID));
        } else {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    UnityAds.load(InterID);

                }
            }, 5000);
        }
    }


}
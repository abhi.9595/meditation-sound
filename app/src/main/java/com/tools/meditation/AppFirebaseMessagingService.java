package com.tools.meditation;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tools.meditation.activities.SplashActivity;

import java.util.Map;
import java.util.Random;

public class AppFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCMService: onMessage";
    public String message = "", title = "", action = "", version = "";
    public String NOTIFICATION_CHANNEL = "myAppChannel";
    public String CHANNEL_ID = "myAppChannel";

    public static int randomNumberInRange() {
        Random random = new Random();
        return random.nextInt((10000 - 5000) + 1) + 5000;
    }

    @SuppressLint("WrongThread")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO Auto-generated method stub

        //default init
        message = "";
        title = "";
        version = "";
        action = "";

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> allData = remoteMessage.getData();
            for (Map.Entry<String, String> entry : allData.entrySet()) {
                Log.d("message data", entry.getKey() + "  " + entry.getValue());
                switch (entry.getKey()) {

                    case "message":
                        message = entry.getValue();
                        break;

                    case "body":
                        message = entry.getValue();
                        break;

                    case "title":
                        title = entry.getValue();
                        break;

                    case "action":
                        action = entry.getValue();
                        break;

                    case "version":
                        version = entry.getValue();
                        break;
                }
            }
        }

        sendSimpleNotification(this);

    }

    public void createChannel(NotificationManager notificationManager, String type, String value) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel chan1 = new NotificationChannel(type, value, NotificationManager.IMPORTANCE_HIGH);
            chan1.setLightColor(android.graphics.Color.GREEN);
            chan1.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationManager.createNotificationChannel(chan1);
        }
    }

    public void sendSimpleNotification(Context context) {
        try {
            Intent intent;
            Bundle bundle = new Bundle();
            bundle.putString("title", title);
            bundle.putString("message", message);
            intent = new Intent(context, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingSwitchIntent = PendingIntent.getActivity(context, randomNumberInRange(), intent, PendingIntent.FLAG_UPDATE_CURRENT, bundle);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            createChannel(notificationManager, NOTIFICATION_CHANNEL, CHANNEL_ID);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL);
            builder.setAutoCancel(true);
            builder.setContentTitle(title.isEmpty() ? context.getString(R.string.app_name) : title);
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message.isEmpty() ? getString(R.string.app_name) : message));
            builder.setContentText(message.isEmpty() ? getString(R.string.app_name) : message);
            builder.setSmallIcon(getNotificationIcon());
            builder.setContentIntent(pendingSwitchIntent);

//            playSound(builder);
            builder.setVibrate(new long[]{1000, 1000});
            int notificationId = randomNumberInRange();
            notificationManager.notify(notificationId, builder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getNotificationIcon() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return R.mipmap.ic_launcher;
        else
            return R.mipmap.ic_launcher;
    }

//    private void playSound(NotificationCompat.Builder builder) {
//
//        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//        if (sound == null)
//            sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
//        builder.setSound(sound);
//
//    }

    @Override
    public void onNewToken(String s) {
        FirebaseMessaging.getInstance().subscribeToTopic("SoundMachineApp").addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("success", "success topic register");
            }
        });
    }
}

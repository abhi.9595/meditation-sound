package com.tools.meditation.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationCompat.Builder;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.tools.meditation.R;
import com.tools.meditation.activities.MainActivity;
import com.tools.meditation.model.Noise;
import com.tools.meditation.utils.CommonUtils;
import com.tools.meditation.utils.Constants;
import com.tools.meditation.utils.Pref;
import com.tools.meditation.utils.mediaplayer.MediaPlayerController;
import com.tools.meditation.utils.mediaplayer.MediaPlayerController.PlayResult;

import java.util.ArrayList;
import java.util.List;

public class MediaPlayerService extends Service {
    public static final String ACTION_PAUSE = "action_pause";
    public static final String ACTION_PLAY = "action_play";
    public static final String ACTION_STOP = "action_stop";

    public static final String CHANNEL_ID = "soundMachine_channel";
    private static final int NOTIFY_MODE_NONE = 0;
    private static final int NOTIFY_MODE_FOREGROUND = 1;
    private final IBinder mBinder = new LocalBinder();
    public String favoritePlaying = null;

    public Handler handler;

    public boolean isForeground;

    public CountdownListener listener;
    int countdownTime;
    MediaPlayerController mediaController;
    List<Noise> playlist;
    private boolean isPaused;
    private NotificationManagerCompat mNotificationManager;
    private int mNotifyMode = NOTIFY_MODE_NONE;
    Runnable countDownRunable = new Runnable() {
        public void run() {
            countdownTime--;
            Log.e("PANTAG : Countdown => ", countdownTime + "");
            if (listener != null) {
                listener.onTick(countdownTime);
            }
            if (countdownTime == 0) {
                stopCountdown();
                stopAll();
                favoritePlaying = null;
            } else {
                handler.postDelayed(countDownRunable, 1000);
            }
            if (isForeground) {
                updateTime();
            }
        }
    };
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                String stateStr = intent.getExtras().getString("state");
                if (stateStr == null) {
                    return;
                }
                if (stateStr.equals("IDLE")) {
                    resumeAll(true);
                } else {
                    pauseAll(true);
                }
            }
        }
    };

    public static boolean isLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static boolean isJellyBeanMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1;
    }

    public static boolean isOreo() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    public void setCountdownListener(CountdownListener listener2) {
        listener = listener2;
    }

    public int getPlayingCount() {
        return playlist.size();
    }

    public List<Noise> getPlaylist() {
        return playlist;
    }

    public void setCountDownTime(int countDownTime) {
        countdownTime = countDownTime;
        stopCountdown();
        if (countDownTime > 0) {
            handler.postDelayed(countDownRunable, 1000);
        } else if (isForeground) {
            updateTime();
        }
    }

    public boolean isPlaying(Noise noise) {
        return playlist.contains(noise);
    }

    public boolean isPlaying() {
        return !playlist.isEmpty();
    }

    public void onDestroy() {
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    private void init() {
        registerReceiver(receiver, new IntentFilter("android.intent.action.PHONE_STATE"));
        playlist = new ArrayList();
        handler = new Handler();
        mNotificationManager = NotificationManagerCompat.from(this);
        createNotificationChannel();
        mediaController = new MediaPlayerController(this, 10);
    }

    private void createNotificationChannel() {
        if (isOreo()) {
            CharSequence name = "SoundMachine";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            manager.createNotificationChannel(mChannel);
        }
    }

    public void stopAll() {
        mediaController.stopAll();
        mNotificationManager.cancel(hashCode());
        mNotifyMode = NOTIFY_MODE_NONE;
        playlist.clear();
        stopForeground(true);
        isForeground = false;
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("stop_all"));
        if (listener != null) {
            listener.onStopAll();
        }
    }

    private final PendingIntent retrievePlaybackAction(final String action) {
        final ComponentName serviceName = new ComponentName(this, MediaPlayerService.class);
        Intent intent = new Intent(action);
        intent.setComponent(serviceName);

        return PendingIntent.getService(this, 0, intent, 0);
    }

    private Notification buildNotification() {
        int playButtonResId = isPaused ? R.drawable.ic_play_arrow_white_24dp : R.drawable.ic_pause_white_24dp;

        PendingIntent clickIntent = PendingIntent.getActivity(
                this, 1,
                new Intent(this, MainActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap artwork = BitmapFactory.decodeResource(getResources(), R.drawable.app_icon_512);

        Builder builder = new Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(artwork)
                .setContentIntent(clickIntent)
                .setContentTitle("Sleep Sounds Machine")
                .setContentText("Playing " + (countdownTime > 0 ? CommonUtils.secondsToTimeString(countdownTime) : "(Never auto off)"))
                .addAction(playButtonResId, "", retrievePlaybackAction(isPaused ? ACTION_PLAY : ACTION_PAUSE))
                .addAction(R.drawable.ic_stop_white_24dp, "", retrievePlaybackAction(ACTION_STOP));

        if (isJellyBeanMR1()) {
            builder.setShowWhen(false);
        }

        if (isLollipop()) {
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            androidx.media.app.NotificationCompat.MediaStyle style = new androidx.media.app.NotificationCompat.MediaStyle()
                    .setShowActionsInCompactView(0, 1);
            builder.setStyle(style);
        }
        if (artwork != null && isLollipop()) {
            builder.setColor(Color.parseColor("#403f4d"));
        }

        if (isOreo()) {
            builder.setColorized(true);
        }

        isForeground = true;
        Notification n = builder.build();
        return n;
    }

    public void setVolume(Noise noise, int volume) {
        favoritePlaying = null;
        float realVolume = ((float) volume) / 100.0f;
        for (Noise n : playlist) {
            if (noise.equals(n)) {
                mediaController.setVolume(noise.soundResId, realVolume);
                Pref.setInt(this, Constants.PREF_PREFIX_VOLUME + noise.soundResId, volume);
                n.volume = volume;
                return;
            }
        }
    }


    public void stopCountdown() {
        try {
            handler.removeCallbacks(countDownRunable);
        } catch (Exception e) {
        }
    }

    public PlayResult play(Noise noise) {
        favoritePlaying = null;
        int volume = Pref.getInt(this, Constants.PREF_PREFIX_VOLUME + noise.soundResId, 50);
        noise.volume = volume;
        try {
            PlayResult result = mediaController.play(noise.soundResId, ((float) volume) / 100.0f);
            if (result == PlayResult.PLAY) {
                Noise.addToList(playlist, noise);
                if (playlist.size() == 1) {
                    updateNotification();
                }
                if (isPaused()) {
                    resumeAll(true);
                }
            } else if (result == PlayResult.STOP) {
                Noise.removeFromToList(playlist, noise);
                if (playlist.isEmpty()) {
                    stopForeground(true);
                    isForeground = false;
                }
            }
            Log.e("TAG", "played sound Successfully");
            return result;
        } catch (Exception exc) {
            exc.printStackTrace();
            Log.e("TAG", "Error in playing favorited sounds : " + exc.toString());
            return null;
        }
    }

    public boolean playFavorite(String favoriteId, List<Noise> noises) {
        stopAll();
        if (favoriteId.equals(favoritePlaying)) {
            favoritePlaying = null;
            return false;
        }
        for (Noise n : noises) {
            Pref.setInt(this, Constants.PREF_PREFIX_VOLUME + n.soundResId, n.volume);
            play(n);
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("favorite_play"));
        favoritePlaying = favoriteId;
        if (listener != null) {
            listener.onPlayListChanged();
        }
        return true;
    }

    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public boolean resumeAll(boolean notiToMain) {
        isPaused = false;
        if (playlist.isEmpty()) {
            return false;
        }
        mediaController.resumeAll();
        updateNotification();
        if (notiToMain && listener != null) {
            listener.onResumeAll();
        }
        return true;
    }

    private void updateNotification() {
        final int newNotifyMode;
        if (isPlaying()) {
            newNotifyMode = NOTIFY_MODE_FOREGROUND;
        } else {
            newNotifyMode = NOTIFY_MODE_NONE;
        }

        int notificationId = hashCode();
        if (mNotifyMode != newNotifyMode) {
            if (mNotifyMode == NOTIFY_MODE_FOREGROUND) {
                stopForeground(newNotifyMode == NOTIFY_MODE_NONE);
            } else if (newNotifyMode == NOTIFY_MODE_NONE) {
                mNotificationManager.cancel(notificationId);
            }
        }

        if (newNotifyMode == NOTIFY_MODE_FOREGROUND) {
            startForeground(notificationId, buildNotification());
        }

        mNotifyMode = newNotifyMode;
    }

    public boolean pauseAll(boolean notiToMain) {
        isPaused = true;
        if (playlist.isEmpty()) {
            return false;
        }
        mediaController.pauAll();
        updateNotification();
        if (!notiToMain || listener == null) {
            return true;
        }
        listener.onPauseAll();
        return true;
    }

    private void handleIntent(Intent intent) {
        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();
            if (action.equalsIgnoreCase(ACTION_PLAY)) {
                resumeAll(true);
            } else if (action.equalsIgnoreCase(ACTION_PAUSE)) {
                pauseAll(true);
            } else if (action.equalsIgnoreCase(ACTION_STOP)) {
                stopAll();
                favoritePlaying = null;
            }
        }
    }


    public void updateTime() {
        if (countdownTime > 0) {
            updateNotification();
            return;
        }
        updateNotification();
    }

    public void onCreate() {
        super.onCreate();
        init();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        handleIntent(intent);
        return super.onStartCommand(intent, flags, startId);
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public interface CountdownListener {
        void onPauseAll();

        void onPlayListChanged();

        void onResumeAll();

        void onStopAll();

        void onTick(int i);
    }

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        public MediaPlayerService getService() {
            return MediaPlayerService.this;
        }
    }
}
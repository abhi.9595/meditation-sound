package com.tools.meditation.utils;

import android.content.Context;
import android.content.SharedPreferences.Editor;

public class Pref {
    private static final String PREF_FILE = "PREF";

    public static void setString(Context context, String key, String value) {
        Editor editor = context.getSharedPreferences(PREF_FILE, 0).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void setInt(Context context, String key, int value) {
        Editor editor = context.getSharedPreferences(PREF_FILE, 0).edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void setBoolean(Context context, String key, boolean value) {
        Editor editor = context.getSharedPreferences(PREF_FILE, 0).edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void setLong(Context context, String key, long value) {
        Editor editor = context.getSharedPreferences(PREF_FILE, 0).edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public static String getString(Context context, String key, String defValue) {
        return context.getSharedPreferences(PREF_FILE, 0).getString(key, defValue);
    }

    public static long getLong(Context context, String key, long defValue) {
        return context.getSharedPreferences(PREF_FILE, 0).getLong(key, defValue);
    }

    public static int getInt(Context context, String key, int defValue) {
        return context.getSharedPreferences(PREF_FILE, 0).getInt(key, defValue);
    }

    public static boolean getBoolean(Context context, String key, boolean defValue) {
        return context.getSharedPreferences(PREF_FILE, 0).getBoolean(key, defValue);
    }

    public static boolean isConstain(Context context, String key) {
        return context.getSharedPreferences(PREF_FILE, 0).contains(key);
    }
}

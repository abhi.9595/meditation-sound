package com.tools.meditation.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.inputmethod.InputMethodManager;

import com.tools.meditation.R;

public class CommonUtils {
    public static String secondsToTimeString(int totalSecs) {
        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;
        if (hours > 0) {
            return String.format("%02d:%02d:%02d", new Object[]{Integer.valueOf(hours), Integer.valueOf(minutes), Integer.valueOf(seconds)});
        }
        return String.format("%02d:%02d", new Object[]{Integer.valueOf(minutes), Integer.valueOf(seconds)});
    }

    public static void hideSoftKeyboard(Activity c) {
        if (c.getCurrentFocus() != null) {
            ((InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(c.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void openGGplay(Activity act, String appPackage) {
        try {
            act.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + act.getString(R.string.txt_package_app_2))));
        } catch (ActivityNotFoundException e) {
            act.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(act.getString(R.string.txt_open_app_2))));
        }
    }
}

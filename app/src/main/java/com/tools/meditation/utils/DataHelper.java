package com.tools.meditation.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "data";
    private static final int DATABASE_VERSION = 1;
    private static final String f66ID = "_id";
    private static final String LIST_SOUND = "list_sound";
    private static final String NAME = "name";
    private static final String TABLE_FAVORITES = "favorites";

    public DataHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE favorites(_id INTEGER PRIMARY KEY,name TEXT,list_sound TEXT)");
    }

    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS favorites");
            onCreate(db);
        }
    }
}

package com.tools.meditation.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;

import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.tools.meditation.R;

public class InterstitialAdController {
    private static InterstitialAdController instance;
    private int count = -1;
    private static Context context;
    private InterstitialAd mInterstitialAd;

    public static InterstitialAdController getInstance(Context c) {
        if (instance == null) {
            instance = new InterstitialAdController();
        }
        context = c;
        return instance;
    }

    public void loadAds(Context context) {
        int purchaseStatus = Pref.getInt(context, Constants.PREF_PURCHASED, 1);
        if (purchaseStatus != 2) {
            AdRequest adRequest = new AdRequest.Builder().build();
            InterstitialAd.load(context, context.getResources().getString(R.string.admob_interstitial_ads_id), adRequest,
                    new InterstitialAdLoadCallback() {
                @Override
                public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                    // The mInterstitialAd reference will be null until
                    // an ad is loaded.
                    mInterstitialAd = interstitialAd;
                    Log.i("AdController", "onAdLoaded");
                }

                @Override
                public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                    // Handle the error
                    Log.i("AdController", loadAdError.getMessage());
                    mInterstitialAd = null;
                }

            });

        }
    }

    private void setListner() {
        mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback(){
            @Override
            public void onAdDismissedFullScreenContent() {
                // Called when fullscreen content is dismissed.
                Log.d("TAG", "The ad was dismissed.");
                loadAds(context);
            }

            @Override
            public void onAdFailedToShowFullScreenContent(AdError adError) {
                // Called when fullscreen content failed to show.
                Log.d("TAG", "The ad failed to show.");
                loadAds(context);
            }

            @Override
            public void onAdShowedFullScreenContent() {
                // Called when fullscreen content is shown.
                // Make sure to set your reference to null so you don't
                // show it a second time.
                mInterstitialAd = null;
                Log.d("TAG", "The ad was shown.");
            }
        });
    }

    public void showAds(Activity activity) {
        count++;
        if ((count % 3 == 0)) {
            if (mInterstitialAd != null) {
                setListner();
                mInterstitialAd.show(activity);
            } else {
                Log.d("___ads", "The interstitial wasn't loaded yet.");
            }
        } else {
            Log.d("___ads", "Count not valid");
        }
    }
}

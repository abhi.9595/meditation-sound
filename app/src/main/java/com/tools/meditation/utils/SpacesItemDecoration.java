package com.tools.meditation.utils;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int headerNum;
    private boolean includeEdge;
    private int spacing;
    private int spanCount;

    public SpacesItemDecoration(int spanCount2, int spacing2, boolean includeEdge2, int headerNum2) {
        spanCount = spanCount2;
        spacing = spacing2;
        includeEdge = includeEdge2;
        headerNum = headerNum2;
    }

    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view) - headerNum;
        if (position >= 0) {
            int column = position % spanCount;
            if (includeEdge) {
                outRect.left = spacing - ((spacing * column) / spanCount);
                outRect.right = ((column + 1) * spacing) / spanCount;
                if (position < spanCount) {
                    outRect.top = spacing;
                }
                outRect.bottom = spacing;
                return;
            }
            outRect.left = (spacing * column) / spanCount;
            outRect.right = spacing - (((column + 1) * spacing) / spanCount);
            if (position >= spanCount) {
                outRect.top = spacing;
                return;
            }
            return;
        }
        outRect.left = 0;
        outRect.right = 0;
        outRect.top = 0;
        outRect.bottom = 0;
    }
}

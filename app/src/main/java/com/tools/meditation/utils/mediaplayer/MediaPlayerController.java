package com.tools.meditation.utils.mediaplayer;

import android.content.Context;
import android.media.MediaPlayer;
import java.util.HashMap;

public class MediaPlayerController {
    private Context mContext;
    private HashMap<Integer, MediaPlayer> mediaPlayerMap = new HashMap<>();
    private int poolSize = 10;

    public enum PlayResult {
        PLAY,
        STOP,
        OUT_OF_POOL
    }

    public MediaPlayerController(Context context) {
        mContext = context;
    }

    public MediaPlayerController(Context context, int poolSize2) {
        poolSize = poolSize2;
        mContext = context;
    }

    public PlayResult play(int soundId, float volume) {
        if (mediaPlayerMap.containsKey(Integer.valueOf(soundId))) {
            MediaPlayer mediaPlayer = (MediaPlayer) mediaPlayerMap.get(Integer.valueOf(soundId));
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayerMap.remove(Integer.valueOf(soundId));
            return PlayResult.STOP;
        } else if (mediaPlayerMap.size() >= poolSize) {
            return PlayResult.OUT_OF_POOL;
        } else {
            MediaPlayer mediaPlayer2 = MediaPlayer.create(mContext, soundId);
            mediaPlayer2.setLooping(true);
            mediaPlayer2.setWakeMode(mContext, 1);
            mediaPlayer2.setVolume(volume, volume);
            mediaPlayer2.start();
            mediaPlayerMap.put(Integer.valueOf(soundId), mediaPlayer2);
            return PlayResult.PLAY;
        }
    }

    public void setVolume(int soundId, float volume) {
        if (mediaPlayerMap.containsKey(Integer.valueOf(soundId))) {
            ((MediaPlayer) mediaPlayerMap.get(Integer.valueOf(soundId))).setVolume(volume, volume);
        }
    }

    public void pauAll() {
        for (MediaPlayer m : mediaPlayerMap.values()) {
            m.pause();
        }
    }

    public void resumeAll() {
        for (MediaPlayer m : mediaPlayerMap.values()) {
            int pos = m.getCurrentPosition();
            m.start();
            m.seekTo(pos);
        }
    }

    public void stopAll() {
        for (MediaPlayer m : mediaPlayerMap.values()) {
            m.stop();
            m.release();
        }
        mediaPlayerMap.clear();
    }
}

package com.tools.meditation.utils;

public interface Constants {

    public static final int DEFAULT_VOLUME = 50;
    public static final boolean LOG_ENABLE = true;
    public static final int NO_PURCHASED = 3;
    public static final int PURCHASED = 2;
    public static final int UNKOWN = 1;
    public static final String PREF_IS_FIRST_TIME = "PREF_IS_FIRST_TIME";
    public static final String PREF_IS_GENERATED_FAVORITE = "PREF_IS_GENERATED_FAVORITE";
    public static final String PREF_NOTIFICATION_ACTIVE = "PREF_NOTIFICATION_ACTIVE";
    public static final String PREF_PAGE_SELECTED = "PREF_PAGE_SELECTED";
    public static final String PREF_PREFIX_VOLUME = "volume_";
    public static final String PREF_PURCHASED = "PREF_PURCHASED";
    public static final String PREF_TIME_PICKER_POS = "PREF_TIME_PICKER_POS";
}

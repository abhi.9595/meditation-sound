package com.tools.meditation;

import com.tools.meditation.model.Noise;
import java.util.ArrayList;
import java.util.List;

public class NoiseListCreator {
    public static List<Noise> createRainList() {
        List<Noise> ls = new ArrayList<>();
        ls.add(new Noise("Umbrella", R.drawable.icon_rain_umbrella, R.raw.rain_umbrella, R.drawable.noise_item_2_selector));
        ls.add(new Noise("Thunder", R.drawable.icon_rain_thunders, R.raw.rain_thunders, R.drawable.noise_item_3_selector));
        ls.add(new Noise("Light rain", R.drawable.icon_rain_light, R.raw.rain_light, R.drawable.noise_item_4_selector));
        ls.add(new Noise("Heavy rain", R.drawable.icon_rain_heavy, R.raw.rain_heavy, R.drawable.noise_item_5_selector));
        ls.add(new Noise("Tent", R.drawable.icon_rain_tent, R.raw.rain_tent, R.drawable.noise_item_7_selector));
        ls.add(new Noise("Window", R.drawable.icon_rain_window, R.raw.rain_window, R.drawable.noise_item_8_selector));

        return ls;
    }

    public static List<Noise> createOceanList() {
        List<Noise> ls = new ArrayList<>();
        ls.add(new Noise("Waves", R.drawable.icon_ocean_waves, R.raw.ocean_waves, R.drawable.noise_item_2_selector));
        ls.add(new Noise("Seagulls", R.drawable.icon_ocean_seagulls, R.raw.ocean_seagulls, R.drawable.noise_item_3_selector));
        ls.add(new Noise("Scuba diver", R.drawable.icon_ocean_scuba_diver, R.raw.ocean_diver, R.drawable.noise_item_4_selector));
        ls.add(new Noise("Dolphins", R.drawable.icon_ocean_dolphin, R.raw.ocean_dolphins, R.drawable.noise_item_5_selector));
        ls.add(new Noise("Sailboart", R.drawable.icon_ocean_sailboat, R.raw.ocean_sailboat, R.drawable.noise_item_6_selector));
        ls.add(new Noise("Whale", R.drawable.icon_ocean_whale, R.raw.ocean_whale, R.drawable.noise_item_7_selector));
        return ls;
    }

    public static List<Noise> createRiverList() {
        List<Noise> ls = new ArrayList<>();
        ls.add(new Noise("River", R.drawable.icon_water_river, R.raw.water_river, R.drawable.noise_item_1_selector));
        ls.add(new Noise("Brook", R.drawable.icon_water_brook, R.raw.water_brook, R.drawable.noise_item_1_selector));
        ls.add(new Noise("Creek", R.drawable.icon_water_creek, R.raw.water_creek, R.drawable.noise_item_1_selector));
        ls.add(new Noise("Waterfall", R.drawable.icon_water_waterfall, R.raw.water_waterfall, R.drawable.noise_item_1_selector));
        return ls;
    }

    public static List<Noise> createCityList() {
        List<Noise> ls = new ArrayList<>();
        ls.add(new Noise("Musician", R.drawable.icon_city_musician, R.raw.city_musician, R.drawable.noise_item_1_selector));
        ls.add(new Noise("Coffee shop", R.drawable.icon_city_coffee_shop, R.raw.city_coffee_shop, R.drawable.noise_item_2_selector));
        ls.add(new Noise("Fountain", R.drawable.icon_city_fountain, R.raw.city_fountain, R.drawable.noise_item_2_selector));
        ls.add(new Noise("Children", R.drawable.icon_city_children, R.raw.city_children, R.drawable.noise_item_3_selector));
        ls.add(new Noise("Traffic", R.drawable.icon_city_traffic, R.raw.city_traffic, R.drawable.noise_item_4_selector));
        ls.add(new Noise("Road work", R.drawable.icon_city_road_work, R.raw.city_works, R.drawable.noise_item_6_selector));
        return ls;
    }

    public static List<Noise> createHomeList() {
        List<Noise> ls = new ArrayList<>();
        ls.add(new Noise("Fan", R.drawable.icon_home_fan, R.raw.home_fan, R.drawable.noise_item_1_selector));
        ls.add(new Noise("Hairdryer", R.drawable.icon_home_hairdryer, R.raw.home_hairdryer, R.drawable.noise_item_3_selector));
        ls.add(new Noise("Vacuum cleaner", R.drawable.icon_home_vacuum_cleaner, R.raw.home_vacuum_cleaner, R.drawable.noise_item_4_selector));
        ls.add(new Noise("Cat purring", R.drawable.icon_home_cat_purring, R.raw.home_cat_purring, R.drawable.noise_item_5_selector));
        ls.add(new Noise("Washing machine", R.drawable.icon_home_washing_machine, R.raw.home_washing_machine, R.drawable.noise_item_7_selector));
        ls.add(new Noise("Refrigerator", R.drawable.icon_home_fridge, R.raw.home_fridge, R.drawable.noise_item_9_selector));
        return ls;
    }

    public static List<Noise> createRelaxingList() {
        List<Noise> ls = new ArrayList<>();
        ls.add(new Noise("Piano", R.drawable.icon_music_piano, R.raw.music_piano, R.drawable.noise_item_1_selector));
        ls.add(new Noise("Guitar", R.drawable.icon_music_guitar, R.raw.music_guitar, R.drawable.noise_item_2_selector));
        ls.add(new Noise("Violin", R.drawable.icon_music_violin, R.raw.music_violin, R.drawable.noise_item_3_selector));
        ls.add(new Noise("Harp", R.drawable.icon_music_harp, R.raw.music_harp, R.drawable.noise_item_4_selector));
        ls.add(new Noise("Flute", R.drawable.icon_music_flute, R.raw.music_flute, R.drawable.noise_item_5_selector));
        ls.add(new Noise("Sax", R.drawable.icon_music_sax, R.raw.music_sax, R.drawable.noise_item_5_selector));
        return ls;
    }

    public static List<Noise> createCountrysideList() {
        List<Noise> ls = new ArrayList<>();
        ls.add(new Noise("Blackbirds", R.drawable.icon_nature_day_birds, R.raw.nature_day_blackbirds, R.drawable.noise_item_1_selector));
        ls.add(new Noise("Crows", R.drawable.icon_nature_day_crow, R.raw.nature_day_crows, R.drawable.noise_item_2_selector));
        ls.add(new Noise("Farm", R.drawable.icon_nature_day_farm, R.raw.nature_day_farm, R.drawable.noise_item_3_selector));
        ls.add(new Noise("Cowbells", R.drawable.icon_nature_day_cowbells, R.raw.nature_day_cowbells, R.drawable.noise_item_4_selector));
        ls.add(new Noise("Sheep", R.drawable.icon_nature_day_sheep, R.raw.nature_day_sheep, R.drawable.noise_item_5_selector));
        ls.add(new Noise("Horse", R.drawable.icon_nature_day_horse, R.raw.nature_day_horse, R.drawable.noise_item_6_selector));
        ls.add(new Noise("Eagle", R.drawable.icon_nature_day_eagle, R.raw.nature_day_eagle, R.drawable.noise_item_7_selector));
        return ls;
    }

    public static List<Noise> createNightList() {
        List<Noise> ls = new ArrayList<>();
        ls.add(new Noise("Crickets", R.drawable.icon_nature_night_cricket, R.raw.nature_night_crickets, R.drawable.noise_item_1_selector));
        ls.add(new Noise("Grasshoppers", R.drawable.icon_nature_night_grasshoppers, R.raw.nature_night_grasshoppers, R.drawable.noise_item_2_selector));
        ls.add(new Noise("Owls", R.drawable.icon_nature_night_owl, R.raw.nature_night_owls, R.drawable.noise_item_3_selector));
        ls.add(new Noise("Wolves", R.drawable.icon_nature_night_wolf, R.raw.nature_night_wolves, R.drawable.noise_item_4_selector));
        ls.add(new Noise("Loon", R.drawable.icon_nature_night_loons, R.raw.nature_night_loons, R.drawable.noise_item_5_selector));
        ls.add(new Noise("Coyote", R.drawable.icon_nature_night_coyote, R.raw.nature_night_coyote, R.drawable.noise_item_6_selector));
        return ls;
    }

    public static List<Noise> createOrientalList() {
        List<Noise> ls = new ArrayList<>();
        ls.add(new Noise("Bowls", R.drawable.icon_oriental_bowl, R.raw.oriental_bowls, R.drawable.noise_item_1_selector));
        ls.add(new Noise("Gong", R.drawable.icon_oriental_gong, R.raw.oriental_gong, R.drawable.noise_item_2_selector));
        ls.add(new Noise("Bells", R.drawable.icon_oriental_bell, R.raw.oriental_bells, R.drawable.noise_item_3_selector));
        ls.add(new Noise("Om", R.drawable.icon_oriental_om, R.raw.oriental_om, R.drawable.noise_item_4_selector));
        ls.add(new Noise("Flute", R.drawable.icon_oriental_flute, R.raw.oriental_flute, R.drawable.noise_item_5_selector));
        ls.add(new Noise("Didgeridoo", R.drawable.icon_oriental_didgeridoo, R.raw.oriental_didgeridoo, R.drawable.noise_item_6_selector));
        return ls;
    }

    public static List<Noise> createWindAndFireList() {
        List<Noise> ls = new ArrayList<>();
        ls.add(new Noise("Light wind", R.drawable.icon_air_light_wind, R.raw.air_light_wind, R.drawable.noise_item_1_selector));
        ls.add(new Noise("Strong wind", R.drawable.icon_air_strong_wind, R.raw.air_strong_wind, R.drawable.noise_item_2_selector));
        ls.add(new Noise("Mountain wind", R.drawable.icon_air_wind_mountain, R.raw.air_wind_mountain, R.drawable.noise_item_3_selector));
        ls.add(new Noise("Under the door", R.drawable.icon_air_wind_door, R.raw.air_wind_door, R.drawable.noise_item_4_selector));
        ls.add(new Noise("Campfire", R.drawable.icon_fire_campfire, R.raw.fire_campfire, R.drawable.noise_item_5_selector));
        ls.add(new Noise("Fireplace", R.drawable.icon_fire_fireplace, R.raw.fire_fireplace, R.drawable.noise_item_6_selector));
        return ls;
    }
}
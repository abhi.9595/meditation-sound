package com.tools.meditation.model;

import com.orm.SugarRecord;
import java.util.List;

public class Noise extends SugarRecord {

    public int bgResId;
    public String favoriteId;
    public int iconResId;
    public String name;
    public int soundResId;
    public int volume = -1;

    public Noise() {
    }

    public Noise(String name2, int iconResId2, int soundResId2, int bgResId2) {
        this.name = name2;
        this.iconResId = iconResId2;
        this.soundResId = soundResId2;
        this.bgResId = bgResId2;
    }

    public Noise(String name2, int iconResId2, int soundResId2, int volume2, String favoriteId2) {
        this.name = name2;
        this.iconResId = iconResId2;
        this.soundResId = soundResId2;
        this.volume = volume2;
        this.favoriteId = favoriteId2;
    }

    public static void addToList(List<Noise> ls, Noise target) {
        boolean isContain = false;
        for (Noise n : ls) {
            if (n.equals(target)) {
                isContain = true;
            }
        }
        if (!isContain) {
            ls.add(target);
        }
    }

    public static void removeFromToList(List<Noise> ls, Noise target) {
        for (Noise n : ls) {
            if (n.equals(target)) {
                ls.remove(n);
                return;
            }
        }
    }

    public static List<Noise> getFavoriteNoise(String favoriteId2) {
        return find(Noise.class, "FAVORITE_ID = ?", favoriteId2);
    }

    /*public boolean equals(Object obj) {
        return this.soundResId == ((Noise) obj).soundResId;
    }*/
}
package com.tools.meditation.model;

import com.orm.SugarRecord;

import java.util.List;
import java.util.UUID;

public class Favorite extends SugarRecord {
    public String name;
    public String uniqueId;

    public Favorite() {
    }

    public Favorite(String name2) {
        name = name2;
        uniqueId = UUID.randomUUID().toString();
    }

    public static List<Noise> getNoisesByFavoritesId(int id) {
        List<Noise> fdad = Noise.find(Noise.class, "id = ?", id + "");
        return fdad;
    }

    public void deleteFavorite() {
        delete();
        Noise.deleteAll(Noise.class, "FAVORITE_ID = ?", getId() + "");
    }
}
